package ohh;

import java.util.HashMap;
import java.util.Map;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;

@SpringBootApplication(exclude = { 
	MongoAutoConfiguration.class, MongoDataAutoConfiguration.class
})
public class Main {

	public static Global global = new Global();
	
	public static void main(String[] args) {
		
		// load
		if (!global.load()) {
			System.out.println("Error on " + global.CONFIG_PATH + ": " + global.error());
			System.exit(0);
			return;
		}

		// shutdown hook
		Runtime.getRuntime().addShutdownHook(global.end);
		
		// spring-boot
		SpringApplication app = new SpringApplication(Main.class);
		Map<String, Object> properties = new HashMap<>();
		
		// port
		properties.put("server.port", global.port());
		
		// upload-multipart
//		properties.put("spring.servlet.multipart.max-file-size", global.getMaxFileSize());
//		properties.put("spring.servlet.multipart.max-request-size", global.getMaxFileSize());

		app.setDefaultProperties(properties);
		app.run(args);
		
		// start
		global.start();
		
	}
}
