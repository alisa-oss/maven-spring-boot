package ohh;

public class End extends Thread {
	
	public End(Global global) {
		
		this._global = global;
	}
	
	@Override
	public void run() {
		
		this._global.log("System | ended");
		this._ended = true;
	}
	
	private Global _global = null;
	
	public boolean isEnded() {  return this._ended; }
	private boolean _ended = false;
}
