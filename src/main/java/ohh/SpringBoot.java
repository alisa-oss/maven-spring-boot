package ohh;

import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringBoot {

	Global global = Main.global;

	@RequestMapping(value = "/", 
			  method = { RequestMethod.GET }, 
			  produces = "text/plain; charset=UTF-8")
	public String root() {
		
		return About.NAME + " " + About.VERSION + " (" + About.UPDATED + ")";
	}
	
	@RequestMapping(value = "/version", 
			  method = { RequestMethod.GET }, 
			  produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> version() {
		
		String result = "{ \"version\": \"" + About.VERSION + "\"" 
				  + ", \"updated\": \"" + About.UPDATED + "\" }";
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/config", 
			  method = { RequestMethod.GET }, 
			  produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> config() {
		
		String result = "{ \"version\": \"" + global.config_version() + "\"" 
				  + ", \"updated\": \"" + global.config_updated()+ "\" }";
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/about", 
			  method = { RequestMethod.GET }, 
			  produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> about() {
		
		JSONObject root_obj = new JSONObject();
		root_obj.put("version", About.VERSION);
		root_obj.put("updated", About.UPDATED);
		root_obj.put("os", global.os());
		root_obj.put("java", global.java());
		root_obj.put("started", global.started());
		
		JSONObject config_obj = new JSONObject();
		config_obj.put("version", global.config_version());
		config_obj.put("updated", global.config_updated());
		root_obj.put("config", config_obj);
		
		return ResponseEntity.ok(root_obj.toString());
	}	

	
	// --------------------------- Examples -----------------------------
	
	// PathVariable
	@RequestMapping(value = "/path/{variable}", 
			  method = { RequestMethod.GET }, 
			  produces = "text/plain; charset=UTF-8")
	public ResponseEntity<String> content(@PathVariable String variable) {
		
		String html = "hello " + variable;

		return ResponseEntity.ok(html);
	}
	
	// RequestBody
	@RequestMapping(value = "/body", 
			  method = { RequestMethod.GET }, 
			  produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> body(@RequestBody String json) {
						
		return ResponseEntity.ok(json);
	}
	
	// RequestParam (?id={})
	// body: json
	@RequestMapping(value = "/post", 
			  method = { RequestMethod.POST }, 
			  produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> post(@RequestParam String id, @RequestBody String json) {

		return ResponseEntity.ok(id + "\r\n" + json);
	}
	
	// Error
	@RequestMapping(value = "/error500", 
			  method = { RequestMethod.GET },
			  produces = "application/json; charset=UTF-8")
	public ResponseEntity<String> error500() {
		
		return ResponseEntity.status(500).body("Error 500");
	}
	

	private String _textToHtml(String text) {

		text = text.replaceAll("&", "&amp;");
		text = text.replaceAll("\\\\", "&#92;");
		text = text.replaceAll("\"", "&quot;");
		text = text.replaceAll("'", "&#39;");
		text = text.replaceAll("<", "&lt;");
		text = text.replaceAll(">", "&gt;");
		text = text.replaceAll("\r\n", "<br />");
		text = text.replaceAll("\n", "<br />");

		return text;
	}
}
