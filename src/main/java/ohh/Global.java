package ohh;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class Global {

	public final String CONFIG_PATH = "config.json";

	public boolean load() {

		JSONObject root = alisa.json.Parser.loadObject(CONFIG_PATH);
		if (root == null) { this._error = "parsing error"; return false; }

		// version
		try {
			this._config_version = root.getString("version");
			//if (!this._config_version.equals("1.0")) { this._error = "incorrect version (" + this._config_version + ")"; return false; }
		} 
		catch (JSONException ex) { this._error = "version"; return false; }
		
		// updated
		try { this._config_updated = root.getString("updated"); } 
		catch (JSONException ex) { this._error = "updated"; return false; }

		// port (int)
		try { this._port = root.getInt("port"); } 
		catch (JSONException ex) { this._error = "port"; return false; }
		
		// cors (String) - option
		try { this._cors = root.getString("cors"); } 
		catch (JSONException ex) { }
		

		// log (string)
		try { 
			this._log_path = root.getString("log"); 
			char c = this._log_path.charAt(this._log_path.length() - 1);
			if (c == '/' || c == '\\') {
				this._log_path = this._log_path.substring(0, this._log_path.length() - 1);
			}
		} 
		catch (JSONException ex) { this._error = "log"; return false; }		
		
		return true;
	}

	public void start() {

		this.log("-------------------------------------------------------------------------------");
		this.log(About.NAME + " " + About.VERSION + " (" + About.UPDATED + ")");

		// config
		this.log("System | config | " + this._config_version + " (" + this._config_updated + ")");

		// system started
		this.log("System | started | " + this._started.toPreciseString());		
		
		// os
		try { this._os = System.getProperty("os.name"); }
		catch (Exception ex) { }
		this.log("System | os | " + this._os);
		
		// java
		try {
			String vendor = System.getProperty("java.vendor");
			String name = System.getProperty("java.vm.name");
			String version = System.getProperty("java.version");
			String model = System.getProperty("sun.arch.data.model");
			this._java = name + " " + version + " (" + model + "-Bit), " + vendor;
		}
		catch (Exception ex) { }
		this.log("System | java | " + this._java);
				
		while (true) {
			alisa.DateTime datetime = new alisa.DateTime();        

			// delay
			try { Thread.sleep(1000); }  
			catch (InterruptedException ex) { Thread.currentThread().interrupt(); }

			if (this.end.isEnded()) break;
		}	
	}

	public boolean log(String content) {

		alisa.DateTime now = new alisa.DateTime();
		int year = now.getYear();
		int month = now.getMonth();
		int day = now.getDay();

		String path = this._log_path + "/" + year;
		if (month < 10) { path += "/0" + month; } 
		else { path += "/" + month; }

		if (day < 10) { path += "/0" + day; } 
		else { path += "/" + day; }

		File file = new File(path);
		if (!file.exists()) { file.mkdirs(); }

		String log_file_path = file + "/" + About.NAME + ".txt";

		// write log        
		try (FileWriter writer = new FileWriter(log_file_path, true)) { // true = Append, false = Overwrite
			writer.write(now.toPreciseString()+ "\t\t" + content + "\r\n");
		} 
		catch (IOException ex) { return false; }

		// output to display
		System.out.println(now.toPreciseString() + "\t\t" + content);

		return true;
	}

	
	// ---------------- Attributes --------------------    
	
	public final End end = new End(this);
	
	public int port() { return this._port; }
	private int _port = -1;

	public String cors() { return this._cors; }
	private String _cors = "*";
	
	public String log_path() { return this._log_path; }
	private String _log_path = "";
	
	public String error() { return this._error; }
	private String _error = "";
	
	public String config_version() { return this._config_version; }
	private String _config_version = "";
	
	public String config_updated() { return this._config_updated; }
	private String _config_updated = "";
	
	public alisa.DateTime started() { return this._started; }
	private alisa.DateTime _started = new alisa.DateTime();
	
	public String os() { return this._os; }
	private String _os = "";
	
	public String java() { return this._java; }
	private String _java = "";	
	
	// ---------------- Variables ------------------
	
}
